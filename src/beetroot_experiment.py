from r12_control.arm import *
import time
import re;
from gripper import Gripper
import numpy as np

USER_ANSWERS = ['y', 'Y', 'n', 'N']
CAMERA_PARAMS = [640, 480]
WORKSPACE = (590, 325) #mm

def px_to_mm(pos):
    return (((pos[0]-(CAMERA_PARAMS[0]/2))/CAMERA_PARAMS[0])*WORKSPACE[0],
            ((pos[1]-(CAMERA_PARAMS[1]/2))/CAMERA_PARAMS[1])*WORKSPACE[1])


class BeetRootPicker:
    speed = None
    is_calibrated = None
    robot = None
    gripper = None
    com = None
    exp_home_pos = None
    exp_discard_pos = None


    def __init__(self, robot_COM='COM4', gripper_COM='COM3'):
        self.speed = 2500
        self.com = robot_COM
        self.robot = Arm()
        self.gripper = Gripper(gripper_COM, 9600, 0.01)
        self.initialize()
        self.calibrate_initial()

    def initialize(self):
        self.robot.connect('COM4')
        self.robot.write('START')
        print("Robot starting...")
        time.sleep(2)
        self.robot.write('{} SPEED !'.format(self.speed))
        print("Setting speed value to {}".format(self.speed))
        print(self.robot.read())
        self.gripper.moveGripper(self.gripper.angleMax)

    def calibrate_initial(self):
        while self.is_calibrated not in USER_ANSWERS:
            self.is_calibrated = input("Is the robot in the upright (home) position? [y/n]")
            if self.is_calibrated not in USER_ANSWERS:
                print('Please respond with [y/n]')
            elif self.is_calibrated in ['n', 'N']:
                self.robot.write('DE-ENERGIZE')
                print("\nPlease manually move the robot in the upright position.")
                input("\nWhen done, press enter to continue...")
                self.robot.write('ENERGIZE')
                time.sleep(2)
                self.robot.write('CALIBRATE')
                print(self.robot.read())
                print("Robot ready to operate\n")
        self.is_calibrated = 'y'

    def experiment_set_up(self):
        time.sleep(1)
        self.robot.write('TELL SHOULDER 3000 MOVE')
        print(self.robot.read())
        time.sleep(2)
        self.robot.write('TELL ELBOW 6000 MOVE')
        print(self.robot.read())
        time.sleep(2)
        self.robot.write('TELL L-HAND 2400 MOVE')
        print(self.robot.read())
        time.sleep(2)
        self.robot.write('TELL WRIST 1150 MOVE')
        print(self.robot.read())
        time.sleep(2)
        self.robot.write('COMPUTE CARTWHERE')
        self.exp_home_pos = self.get_position()

    def pick(self):
        time.sleep(2)
        # _, _, current_z = self.get_position()
        # z_togo = (-81.5 - current_z) * 10
        self.robot.write('CARTESIAN 0 0 -1600 MOVE')
        time.sleep(2)
        self.gripper.moveGripper(self.gripper.angleMin)

    def discard(self):
        # _, _, current_z = self.get_position()
        # z_togo = (78.5 - current_z) * 10
        time.sleep(2)
        self.robot.write('CARTESIAN 0 0 1650 MOVE')
        time.sleep(2)
        self.robot.write('JOINT TELL WAIST 3500 MOVE')
        time.sleep(2)
        #open
        self.gripper.moveGripper(self.gripper.angleMax)
        time.sleep(2)
        self.robot.write('JOINT TELL WAIST -3500 MOVE')

    def return_initial(self):
        time.sleep(2)
        self.robot.write('CARTESIAN '+
                         str(self.exp_home_pos[0]*10) + ' ' +
                         str(self.exp_home_pos[1]*10) + ' ' +
                         str(self.exp_home_pos[2]*10) + ' MOVETO')

    def get_position(self):
        time.sleep(2)
        self.robot.write('CARTESIAN WHERE')
        response = self.robot.read()
        return tuple([float(s) for s in re.findall(r'-?\d+\.?\d*', response)][:3])

    def goto(self, x, y, z=None):
        time.sleep(2)
        current_x, current_y, current_z = self.get_position()
        x_togo = (x-current_x)*10
        y_togo = (y-current_y)*10
        if z:
            z_togo = (z-current_z)*10
        else:
            z_togo = 0
        time.sleep(2)
        self.robot.write('CARTESIAN '+ str(int(x_togo)) + ' ' + str(int(y_togo)) + ' ' + str(int(z_togo)) + ' MOVETO')

    def vision_to_workspace(self, pos):
        mm_pos = px_to_mm(pos)
        ws_x = mm_pos[1] + 50
        ws_y = WORKSPACE[0]/2 - mm_pos[0] - 50
        return ws_x, ws_y


if __name__ == '__main__':
    robot_picker = BeetRootPicker('COM4')
    robot_picker.experiment_set_up()
    while True:
        #todo Vision give X, Y

        x_togo, y_togo = robot_picker.vision_to_workspace((200, 150))

        input('Press enter to pick...')
        robot_picker.goto(x_togo, y_togo)
        robot_picker.pick()
        robot_picker.discard()
        robot_picker.goto(0, 344, 78.6)

