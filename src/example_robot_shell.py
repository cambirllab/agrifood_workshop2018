from r12_control.arm import *
import time
import re;
from gripper import Gripper

SPEED = 2500

is_calibrated = None
USER_ANSWERS = ['y', 'Y', 'n', 'N']


def calibrate_initial():
    robot.write('DE-ENERGIZE')
    print("\nPlease manually move the robot in the upright position.")
    input("\nWhen done, press enter to continue...")
    robot.write('ENERGIZE')
    time.sleep(2)
    robot.write('CALIBRATE')
    print(robot.read())
    print("Robot ready to operate\n")


def experiment_set_up():
    time.sleep(1)
    robot.write('TELL SHOULDER 3000 MOVE')
    print(robot.read())
    time.sleep(2)
    robot.write('TELL ELBOW 6000 MOVE')
    print(robot.read())
    time.sleep(2)
    robot.write('TELL L-HAND 2400 MOVE')
    print(robot.read())
    time.sleep(2)
    robot.write('TELL WRIST 2100 MOVE')
    print(robot.read())


def pick(position=None):
    time.sleep(2)
    if position:
        z_togo = (-81.5-position[2])*10
        robot.write('CARTESIAN 0 0 ' + str(z_togo) + ' MOVE')
    else:
        robot.write('CARTESIAN 0 0 -1600 MOVE')


def back_up():
    time.sleep(2)
    robot.write('CARTESIAN 0 211.8 -63.1 MOVETO')


def get_position():
    time.sleep(2)
    robot.write('CARTESIAN WHERE')
    response = robot.read()
    return [float(s) for s in re.findall(r'-?\d+\.?\d*', response)][:3]

if __name__ == "__main__":

    # Initialize arm and gripper
    robot = Arm()
    robot.connect('COM4')
    gripper = Gripper('COM3', 9600, 0.01)

    # Start r12 and set initial speed
    robot.write('START')
    print("Robot starting...")
    time.sleep(2)
    robot.write('{} SPEED !'.format(SPEED))
    print("Setting speed value to {}".format(SPEED))
    print(robot.read())

    try:
        # Calibrate the robot to ensure start from the HOME position
        while is_calibrated not in USER_ANSWERS:
            is_calibrated = input("Is the robot in the upright (home) position? [y/n]")
            if is_calibrated not in USER_ANSWERS:
                print('Please respond with [y/n]')
            elif is_calibrated in ['n','N']:
                calibrate_initial()

        experiment_set_up()
        robot.write('COMPUTE CARTWHERE')
        print(robot.read())
        gripper.moveGripper(gripper.grip)

        # USE  robot.write(#'CARTESIAN X? Y? Z? MOVE') FOR RELATIVE MOVE

        while True:
            cmd = input("write command: ")
            robot.write(cmd)
            time.sleep(.5)
            response = robot.read()
            print(response)

        # while True:
        #     input("pick? ")
        #     pos = get_position()
        #     pick(pos)
        #     time.sleep(.5)
        #     place()
        #     go_home()

        print("\n\nDisconnecting and exiting...")
        robot.disconnect()

    except:
        time.sleep(1)
        robot.write('HOME')
        time.sleep(1)
        print("\n\nDisconnecting and exiting...")
        robot.disconnect()
